import os, pdb, json
import pandas as pd
import numpy as np
import shutil

"""
It creates this all_languages.csv file with all the languages (but it is not well formatted on the web IDE)
"""
keywords = pd.read_csv('all_languages.csv')
print(f"we have {len(keywords)}")

print('THIS WILL GENERATE ALL LANGUAGE CSV!')
pdb.set_trace()
all_languages = keywords.copy()

# languages
lang_text_files = np.sort(os.listdir('i18n')).tolist()
flags = np.sort(os.listdir('flags')).tolist()
assert len(lang_text_files) == len(flags), f"problem, we have {len(lang_text_files)} language files and {len(flags)}, please see what's happening"
print(f'we have {len(flags)} languages:')
for lang in lang_text_files:
    cur_lang = lang[:-5]
    with open(os.path.join('i18n', lang)) as user_file:
        lang_keywords = json.load(user_file)
    num_kw = len(lang_keywords)
    if num_kw == len(keywords):
        print(f'{cur_lang} (with {len(lang_keywords)}, correct)')
        all_languages[cur_lang] = list(lang_keywords.values())
        #pdb.set_trace()
    else:
        print(f'{cur_lang} (with {len(lang_keywords)}, wrong, we need to update this file)')
        if not os.path.exists('backups'):
            os.mkdir('backups')
        # create backup
        shutil.copy(os.path.join('i18n', lang), os.path.join('backups', f"{cur_lang}_outdated.json"))
        # update file
        #pdb.set_trace()
        for kw in lang_keyword:
            if kw not in keywords:
                # add the missing keyword
                pdb.set_trace()
all_languages.to_csv('all_languages.csv')
pdb.set_trace()
